import numpy as np
import matplotlib.pyplot as plt


from gold_code import load_gold_rz,delay
from sk_dsp_comm import sigsys as ss

def do_it(gold_num=1,chip_rate=1e6,sampling_rate=280e6,fc=70e6):
    assert sampling_rate>=2*chip_rate,"Silly Robert, success is for engineers who understand nyquist"
    upsample_factor_float = sampling_rate/chip_rate
    assert upsample_factor_float==int(upsample_factor_float),"Silly Robert, try a integer upsampling factor"
    upsample_factor = int(upsample_factor_float)
    raw_zeros_ones = load_gold_rz(gold_num,bits_out=1023)
    baseband_nrz_gold,_ = ss.NRZ_bits2(raw_zeros_ones,Ns=upsample_factor,pulse='rect',alpha=None,M=None)
    num_samples = len(baseband_nrz_gold)
    tt = np.arange(num_samples)/sampling_rate
    print(tt[-1]*1e3,'ms length of period')
    print(num_samples,'number of samples')
    sinusoid = np.cos(2*np.pi*tt*fc)
    tx_nrz_gold = baseband_nrz_gold*sinusoid
    filename = 'rm_gc{}_{}Mcps_{}Msps_{}MHz'.format(gold_num,int(chip_rate/1e6),int(sampling_rate/1e6),int(fc/1e6))
    filename_zeros = filename+'_zeros'
    print(filename)
    print(filename_zeros)
    tx_nrz_gold_zeros = np.zeros([num_samples*2,])
    tx_nrz_gold_zeros[0:num_samples]=tx_nrz_gold
    np.savetxt(filename+'.csv',tx_nrz_gold, delimiter=',', header='data')
    np.savetxt(filename_zeros+'.csv', tx_nrz_gold_zeros, delimiter=',', header='data')
    return tx_nrz_gold


if __name__ == '__main__':
    gold1 = do_it(gold_num=1)
    gold2 = do_it(gold_num=2)
    delay_chips = 5
    delay_seconds = delay_chips/1e6
    gold2_delay = delay(gold2,delay=delay_seconds,fs=280e6)
    filename = 'rm_gc{}_and_gc{}_{}Mcps_{}Msps_{}MHz_{}us'.format(1,2,1,280,70,int(delay_seconds*1e6))
    print filename
    np.savetxt(filename + '.csv', gold1+gold2_delay, delimiter=',', header='data')
    np.complex64(gold2).tofile('gold2.bin')
    np.complex64(gold2_delay).tofile('gold2_delay.bin')