#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Capture Setup
# Generated: Wed Oct 31 05:19:54 2018
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import ConfigParser
import sip
import sys
import time
from gnuradio import qtgui


class capture_setup(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Capture Setup")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Capture Setup")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "capture_setup")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self._default_tx_on_off_config = ConfigParser.ConfigParser()
        self._default_tx_on_off_config.read('gain.cfg')
        try: default_tx_on_off = self._default_tx_on_off_config.getfloat('main', 'default_tx_on_off')
        except: default_tx_on_off = 0
        self.default_tx_on_off = default_tx_on_off
        self._default_num_samples_config = ConfigParser.ConfigParser()
        self._default_num_samples_config.read('gain.cfg')
        try: default_num_samples = self._default_num_samples_config.getint('main', 'default_num_samples')
        except: default_num_samples = 1048576
        self.default_num_samples = default_num_samples
        self._default_gain_config = ConfigParser.ConfigParser()
        self._default_gain_config.read('gain.cfg')
        try: default_gain = self._default_gain_config.getfloat('main', 'gain')
        except: default_gain = 64
        self.default_gain = default_gain
        self.tx_on_off = tx_on_off = default_tx_on_off
        self.transmit_gain = transmit_gain = 0
        self.samp_rate = samp_rate = 1000000
        self.num_samples = num_samples = default_num_samples
        self.gain = gain = default_gain
        self.fm_freq = fm_freq = 70

        ##################################################
        # Blocks
        ##################################################
        self._gain_range = Range(0, 100, 1, default_gain, 200)
        self._gain_win = RangeWidget(self._gain_range, self.set_gain, 'gain', "counter_slider", float)
        self.top_layout.addWidget(self._gain_win)
        self._fm_freq_range = Range(60, 110, .1, 70, 200)
        self._fm_freq_win = RangeWidget(self._fm_freq_range, self.set_fm_freq, 'fm_freq', "counter_slider", float)
        self.top_layout.addWidget(self._fm_freq_win)
        self.uhd_usrp_source_0 = uhd.usrp_source(
        	",".join(("", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_center_freq(int(fm_freq*1e6), 0)
        self.uhd_usrp_source_0.set_gain(gain, 0)
        self.uhd_usrp_source_0.set_bandwidth(2*samp_rate, 0)
        _tx_on_off_check_box = Qt.QCheckBox("tx_on_off")
        self._tx_on_off_choices = {True: 1.0, False: 0.0}
        self._tx_on_off_choices_inv = dict((v,k) for k,v in self._tx_on_off_choices.iteritems())
        self._tx_on_off_callback = lambda i: Qt.QMetaObject.invokeMethod(_tx_on_off_check_box, "setChecked", Qt.Q_ARG("bool", self._tx_on_off_choices_inv[i]))
        self._tx_on_off_callback(self.tx_on_off)
        _tx_on_off_check_box.stateChanged.connect(lambda i: self.set_tx_on_off(self._tx_on_off_choices[bool(i)]))
        self.top_layout.addWidget(_tx_on_off_check_box)
        self._transmit_gain_range = Range(0, 100, 1, 0, 200)
        self._transmit_gain_win = RangeWidget(self._transmit_gain_range, self.set_transmit_gain, 'transmit_gain', "counter_slider", float)
        self.top_layout.addWidget(self._transmit_gain_win)
        self.qtgui_time_sink_x_0 = qtgui.time_sink_c(
        	1024, #size
        	samp_rate, #samp_rate
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(False)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)

        if not True:
          self.qtgui_time_sink_x_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                if(i % 2 == 0):
                    self.qtgui_time_sink_x_0.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_time_sink_x_0_win)
        self.qtgui_histogram_sink_x_0 = qtgui.histogram_sink_f(
        	1024,
        	100,
                -1,
                1,
        	"",
        	1
        )

        self.qtgui_histogram_sink_x_0.set_update_time(0.10)
        self.qtgui_histogram_sink_x_0.enable_autoscale(True)
        self.qtgui_histogram_sink_x_0.enable_accumulate(False)
        self.qtgui_histogram_sink_x_0.enable_grid(False)
        self.qtgui_histogram_sink_x_0.enable_axis_labels(True)

        if not True:
          self.qtgui_histogram_sink_x_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_histogram_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_histogram_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_histogram_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_histogram_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_histogram_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_histogram_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_histogram_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_histogram_sink_x_0_win = sip.wrapinstance(self.qtgui_histogram_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_histogram_sink_x_0_win)

        self._num_samples_tool_bar = Qt.QToolBar(self)
        self._num_samples_tool_bar.addWidget(Qt.QLabel("num_samples"+": "))
        self._num_samples_line_edit = Qt.QLineEdit(str(self.num_samples))
        self._num_samples_tool_bar.addWidget(self._num_samples_line_edit)
        self._num_samples_line_edit.returnPressed.connect(
        	lambda: self.set_num_samples(int(str(self._num_samples_line_edit.text().toAscii()))))
        self.top_layout.addWidget(self._num_samples_tool_bar)
        self.blocks_complex_to_real_0 = blocks.complex_to_real(1)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_complex_to_real_0, 0), (self.qtgui_histogram_sink_x_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.blocks_complex_to_real_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.qtgui_time_sink_x_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "capture_setup")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_default_tx_on_off(self):
        return self.default_tx_on_off

    def set_default_tx_on_off(self, default_tx_on_off):
        self.default_tx_on_off = default_tx_on_off
        self.set_tx_on_off(self.default_tx_on_off)

    def get_default_num_samples(self):
        return self.default_num_samples

    def set_default_num_samples(self, default_num_samples):
        self.default_num_samples = default_num_samples
        self.set_num_samples(self.default_num_samples)

    def get_default_gain(self):
        return self.default_gain

    def set_default_gain(self, default_gain):
        self.default_gain = default_gain
        self.set_gain(self.default_gain)

    def get_tx_on_off(self):
        return self.tx_on_off

    def set_tx_on_off(self, tx_on_off):
        self.tx_on_off = tx_on_off
        self._tx_on_off_callback(self.tx_on_off)
        self._default_tx_on_off_config = ConfigParser.ConfigParser()
        self._default_tx_on_off_config.read('gain.cfg')
        if not self._default_tx_on_off_config.has_section('main'):
        	self._default_tx_on_off_config.add_section('main')
        self._default_tx_on_off_config.set('main', 'default_tx_on_off', str(self.tx_on_off))
        self._default_tx_on_off_config.write(open('gain.cfg', 'w'))

    def get_transmit_gain(self):
        return self.transmit_gain

    def set_transmit_gain(self, transmit_gain):
        self.transmit_gain = transmit_gain

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_source_0.set_bandwidth(2*self.samp_rate, 0)
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate)

    def get_num_samples(self):
        return self.num_samples

    def set_num_samples(self, num_samples):
        self.num_samples = num_samples
        Qt.QMetaObject.invokeMethod(self._num_samples_line_edit, "setText", Qt.Q_ARG("QString", str(self.num_samples)))
        self._default_num_samples_config = ConfigParser.ConfigParser()
        self._default_num_samples_config.read('gain.cfg')
        if not self._default_num_samples_config.has_section('main'):
        	self._default_num_samples_config.add_section('main')
        self._default_num_samples_config.set('main', 'default_num_samples', str(self.num_samples))
        self._default_num_samples_config.write(open('gain.cfg', 'w'))

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_source_0.set_gain(self.gain, 0)

        self._default_gain_config = ConfigParser.ConfigParser()
        self._default_gain_config.read('gain.cfg')
        if not self._default_gain_config.has_section('main'):
        	self._default_gain_config.add_section('main')
        self._default_gain_config.set('main', 'gain', str(self.gain))
        self._default_gain_config.write(open('gain.cfg', 'w'))

    def get_fm_freq(self):
        return self.fm_freq

    def set_fm_freq(self, fm_freq):
        self.fm_freq = fm_freq
        self.uhd_usrp_source_0.set_center_freq(int(self.fm_freq*1e6), 0)


def main(top_block_cls=capture_setup, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
