import argparse
import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser(description="plot IQ data")
parser.add_argument("--int_time", type=float,default=.002048)
parser.add_argument("--samp_rate", type=float,default=1e6)
args = parser.parse_args()
blah = np.fromfile('gold1_plus_gold2_capture.dat',dtype='complex64')
blah2 = np.fromfile('gold1_plus_gold2_pristine.dat',dtype='complex64')
fs=args.samp_rate
length=int(args.int_time*fs)
fdop = 0
doppler = np.exp(1j * 2 * np.pi * np.arange(length,dtype='float32') / fs * fdop, dtype='complex64')
#blah=blah[:length]*doppler


plt.subplot(211)
plt.plot(blah[:length].real)
#plt.ylim(-2,2)
plt.grid()
plt.subplot(212)
plt.plot(blah[:length].imag)
#plt.ylim(-2,2)
plt.grid()
#plt.figure()
#plt.psd(blah**2,2**17,Fs=fs)
#plt.figure()
#plt.psd(blah**2,2**17,Fs=fs)
plt.show()

