import subprocess
import json
import os
import shutil
import time

import numpy as np
from PySide import QtCore, QtGui, QtUiTools

MainWindow = None # Initialize global
samp_rate = 1e6
RAW_CAP_FNAME = "gold1_plus_gold2_capture.dat"
CAL_CAP_FNAME = "gold1_plus_gold2_capture_cal.dat"

def loadUiWidget(uifilename, parent=None):
    loader = QtUiTools.QUiLoader()
    uifile = QtCore.QFile(uifilename)
    uifile.open(QtCore.QFile.ReadOnly)
    ui = loader.load(uifile, parent)
    uifile.close()
    return ui

def call_make_recipe(recipe,extra_args=dict()):
    try:
        call_string = "make" + " {}".format(recipe)
        for key in extra_args:
            call_string+=" {}='{}'".format(key,extra_args[key])
        print(call_string)
        retcode = subprocess.call(call_string, shell=True)
        if retcode < 0:
            print >>sys.stderr, "Child was terminated by signal", -retcode
    except OSError as e:
        print >>sys.stderr, "Execution failed:", e
def calibrate_helper(raw_file_name,cal_file_name,int_time,samp_rate):
    with open('caf_peak.json','r') as a_file:
        caf_info = json.load(a_file)
    tdoa_s = caf_info['tdoa_s']
    data = np.fromfile(raw_file_name,dtype='complex64')
    data = data[0:int(int_time*samp_rate)]
    print('TDOA is {}'.format(tdoa_s))
    tdoa_samples = -int(round(tdoa_s*1e6))
    print("Shifting circularly {} samples".format(tdoa_samples))
    data_shift = np.roll(data,tdoa_samples)
    print "Rotating circularly with a period of {}".format(int(int_time*samp_rate))
    print "{} -> {}".format(raw_file_name,cal_file_name)
    data_shift.tofile(cal_file_name)

def calibrate_caf_one():
    print "calibrate"
    int_time = MainWindow.integration_time_us.value()*1e-6
    call_make_recipe("calibrate_caf",{"int_time":int_time,"samp_rate":samp_rate})
    calibrate_helper(RAW_CAP_FNAME,CAL_CAP_FNAME,int_time,samp_rate)

def run_caf_one():
    print "Run CAF1"
    int_time = MainWindow.integration_time_us.value()*1e-6
    call_make_recipe("run_caf1",{"int_time":int_time,"samp_rate":samp_rate})
    call_make_recipe("plot_surface")

def run_caf_two():
    print "Run CAF2"
    int_time = MainWindow.integration_time_us.value()*1e-6
    call_make_recipe("run_caf2",{"int_time":int_time,"samp_rate":samp_rate})
    call_make_recipe("plot_surface")

def capture_setup():
    print "Capture Setup"
    if MainWindow.SoftwareDefinedRadioComboBox.currentText() == "ettus":
        call_make_recipe("capture_setup")
    else:
        call_make_recipe("capture_setup_rtlsdr")

def capture_data():
    print "Capture Data"
    if MainWindow.SoftwareDefinedRadioComboBox.currentText() == "ettus":
        call_make_recipe("capture_data")
    else:
        call_make_recipe("capture_data_rtlsdr")

def simulate_data():
    print "Simulate Data"
    call_make_recipe("simulate_data")

def plot_iq():
    print "Plot IQ"
    int_time = MainWindow.integration_time_us.value()*1e-6
    call_make_recipe("plot_iq",{"int_time":int_time,"samp_rate":samp_rate})

def batch_capture():
    num_captures_per_batch = MainWindow.BatchSize.value()
    folder = MainWindow.BatchFolder.text()
    sleep_time = MainWindow.SleepTime.value()
    print folder
    int_time = MainWindow.integration_time_us.value()*1e-6
    if not os.path.exists(folder):
        os.makedirs(folder)
    start = time.time()
    time_axis_seconds = []
    for ii in range(1,num_captures_per_batch+1):
        seconds_since_start = time.time()-start
        if MainWindow.SoftwareDefinedRadioComboBox.currentText() == "ettus":
            call_make_recipe("capture_data")
        else:
            call_make_recipe("capture_data_rtlsdr")
        time_axis_seconds.append(seconds_since_start)
        raw_capture = os.path.join(folder,"{:03d}.dat".format(ii))
        shutil.copyfile(RAW_CAP_FNAME,raw_capture)
        print("Finished run  {} of {}. Sleeping for {} seconds".format(ii,num_captures_per_batch,sleep_time))
        time.sleep(sleep_time)
    time_fname = "time_axis.json"
    with open(os.path.join(folder,time_fname),'w') as a_file:
        a_dict = {"time_axis_seconds":time_axis_seconds}
        json.dump(a_dict,a_file)

def batch_caf():
    num_captures_per_batch = MainWindow.BatchSize.value()
    folder = MainWindow.BatchFolder.text()
    print folder
    int_time = MainWindow.integration_time_us.value()*1e-6
    if not os.path.exists(folder):
        print("Files not captured yet")
        return 
    tdoa_s_list = []
    fdoa_hz_list = []
    SNR_dB_list = []
    for ii in range(1,num_captures_per_batch+1):
        raw_capture = os.path.join(folder,"{:03d}.dat".format(ii))
        cal_capture = os.path.join(folder,"{:03d}_cal.dat".format(ii))
        call_make_recipe("calibrate_caf",{"int_time":int_time,"samp_rate":samp_rate,
                                          "raw_capture_file_name":raw_capture})
        calibrate_helper(raw_capture,cal_capture,int_time,samp_rate)
        call_make_recipe("run_caf2",{"int_time":int_time,"samp_rate":samp_rate,
                                    "cal_capture_file_name":cal_capture})
        with open('caf_peak.json','r') as a_file:
            caf_info = json.load(a_file)
        tdoa_s = caf_info['tdoa_s']
        fdoa_hz = caf_info['fdoa_hz']
        SNR_dB = caf_info['SNR_dB']
        tdoa_s_list.append(tdoa_s)
        fdoa_hz_list.append(fdoa_hz)
        SNR_dB_list.append(SNR_dB)
    print "SNR_dB",SNR_dB_list   
    print "TDOA_us",tdoa_s_list*np.array([1e6])
    print "FDOA_Hz",fdoa_hz_list
    results_fname = "results_{}.json".format(int(int_time*samp_rate))
    with open(os.path.join(folder,results_fname),'w') as a_file:
        a_dict = {"SNR_dB_list":SNR_dB_list,
                  "tdoa_s_list":tdoa_s_list,
                  "fdoa_hz_list":fdoa_hz_list}
        json.dump(a_dict,a_file)

def plot_batch_results():
    folder = MainWindow.BatchFolder.text()
    int_time = MainWindow.integration_time_us.value()*1e-6    
    call_make_recipe("plot_batch_results",{"int_time":int_time,"samp_rate":samp_rate,
                                    "batch_folder":folder})

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = loadUiWidget("gps_caf_gui.ui")
    MainWindow.Calibrate.clicked.connect(calibrate_caf_one)
    MainWindow.RunCAF1.clicked.connect(run_caf_one)
    MainWindow.RunCAF2.clicked.connect(run_caf_two)
    MainWindow.CaptureSetup.clicked.connect(capture_setup)
    MainWindow.CaptureData.clicked.connect(capture_data)
    MainWindow.SimulateData.clicked.connect(simulate_data)
    MainWindow.PlotIQ.clicked.connect(plot_iq)
    MainWindow.BatchCapture.clicked.connect(batch_capture)
    MainWindow.BatchCAF.clicked.connect(batch_caf)
    MainWindow.BatchSize.setValue(3)
    MainWindow.BatchSize.setRange(3,100)
    MainWindow.BatchPlotResults.clicked.connect(plot_batch_results)
    MainWindow.show()
    sys.exit(app.exec_())
