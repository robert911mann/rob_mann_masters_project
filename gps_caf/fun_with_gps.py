import time
from collections import OrderedDict
from skyfield.api import Topos, load
import datetime
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

"""
GPS BIIF-2  (PRN 01)
1 37753U 11036A   18298.05332753  .00000006  00000-0  00000+0 0  9997
2 37753  55.7532  81.0679 0080035  38.8605 147.1314  2.00564880 53272

GPS BIIR-13 (PRN 02)
1 28474U 04045A   18298.48025111 -.00000003  00000-0  00000+0 0  9991
2 28474  54.5557  77.3687 0185340 255.7654 112.9753  2.00574340102434


"""

def get_datetime(satellite):
    epoch_utc_tuple = satellite.epoch.utc
    whole_seconds = int(epoch_utc_tuple[5])
    micro_seconds = int((epoch_utc_tuple[5] - int(epoch_utc_tuple[5])) * 1e6)
    epoch_utc_tuple_us = [epoch_utc_tuple[0], epoch_utc_tuple[1], epoch_utc_tuple[2],
                          epoch_utc_tuple[3], epoch_utc_tuple[4], whole_seconds, micro_seconds]
    epoch_utc_datetime = datetime.datetime(*np.int32(epoch_utc_tuple_us))
    return epoch_utc_datetime


FILTER_BY_ABOVE_HORIZON = False
gps_l1_freq = 1575.42e6
speed_of_light = 299792458.0
ts = load.timescale()
stations_url = 'https://www.celestrak.com/NORAD/elements/gps-ops.txt'
satellites = load.tle(stations_url)

prns = range(1,2)
#del prns[3] # No sate
legend_list = []
for ii in prns:
    if ii+1 <= 10:
        key = 'PRN 0{}'.format(ii)
    else:
        key = 'PRN {}'.format(ii)
    legend_list.append(key)
    satellite = satellites[key]
    dt = get_datetime(satellite)
    geocentric = satellite.at(satellite.epoch)
    # 38.893774, -104.800531, 1971
    eng239 = Topos('38.893774 N', '104.800531 W')
    equator_prime_meridian = Topos('0.0 N', '0.0 W')
    difference = satellite - equator_prime_meridian

    seconds_in_a_day = 24*60*60
    time_step = 10
    time_seconds = np.arange(0,seconds_in_a_day,time_step)
    position_array = np.zeros([len(time_seconds),3])
    velocity_array = np.zeros([len(time_seconds),3])
    distance_array = np.zeros_like(time_seconds,dtype='float32')
    altitude_array = np.zeros_like(time_seconds,dtype='float32')

    tic = time.time()
    for kk, seconds_since_epoch in enumerate(time_seconds):
        new_dt = dt+ datetime.timedelta(seconds=seconds_since_epoch)
        t = ts.utc(*new_dt.timetuple()[0:6])
        topocentric = difference.at(t)
        alt, az, distance = topocentric.altaz()
        if alt.degrees < 0 :
            position_array[kk, :] = topocentric.position.km
            distance_array[kk] = np.linalg.norm(topocentric.position.km)
            velocity_array[kk, :] = topocentric.velocity.km_per_s
        else:
            if FILTER_BY_ABOVE_HORIZON:
                position_array[kk, :] = np.nan,np.nan,np.nan
                distance_array[kk] = np.nan
                velocity_array[kk, :] = np.nan,np.nan,np.nan
            else:
                position_array[kk, :] = topocentric.position.km
                distance_array[kk] = np.linalg.norm(topocentric.position.km)
                velocity_array[kk, :] = topocentric.velocity.km_per_s
        altitude_array[kk] = alt.degrees
    toc = time.time()
    print("Took {}".format(toc-tic))


    distance_array = np.linalg.norm(position_array,axis=1)
    #speed_array = np.linalg.norm(velocity_array,axis=1)
    speed_array = np.diff(distance_array)/time_step
    acceleration_array = np.diff(velocity_array,axis=0)/float(time_step)

    # VT*f/c
    doppler = speed_array*1000.0*gps_l1_freq/speed_of_light


    # plt.plot(time_seconds,position_array[:,0])
    # plt.plot(time_seconds,position_array[:,1])
    # plt.plot(time_seconds,position_array[:,2])
    # plt.grid()
    # plt.xlabel('Time since epoch in s')
    # plt.ylabel('Position in km')
    # plt.legend(['x','y','z'],loc='best')
    # plt.title('Position')
    #
    # plt.figure()
    # plt.plot(time_seconds,distance_array)
    # plt.grid()
    # plt.xlabel('Time since epoch in s')
    # plt.ylabel('Distance in km')
    # plt.title('Distance')
    #
    # plt.figure()
    # plt.plot(time_seconds[:-1],np.diff(position_array[:,0])/float(time_step),'--b')
    # plt.plot(time_seconds[:-1],np.diff(position_array[:,1])/float(time_step),'--g')
    # plt.plot(time_seconds[:-1],np.diff(position_array[:,2])/float(time_step),'--r')
    # plt.plot(time_seconds,velocity_array[:,0],'b')
    # plt.plot(time_seconds,velocity_array[:,1],'g')
    # plt.plot(time_seconds,velocity_array[:,2],'r')
    # plt.grid()
    # plt.xlabel('Time since epoch in s')
    # plt.ylabel('Velocity in km/s')
    # plt.title('Velocity')
    #
    # plt.figure()
    # plt.plot(time_seconds,speed_array)
    # plt.grid()
    # plt.xlabel('Time since epoch in s')
    # plt.ylabel('Speed in km/s^2')
    # plt.title('Speed')
    #
    # plt.figure()
    # plt.plot(time_seconds[:-1],acceleration_array[:,0],'b')
    # plt.plot(time_seconds[:-1],acceleration_array[:,1],'g')
    # plt.plot(time_seconds[:-1],acceleration_array[:,2],'r')
    # plt.grid()
    # plt.xlabel('Time since epoch in s')
    # plt.ylabel('Acceleration in km/s')
    # plt.title('Acceleration')
    #
    # plt.figure()
    # plt.plot(time_seconds[:-1],np.linalg.norm(acceleration_array,axis=1))
    # plt.grid()
    # plt.xlabel('Time since epoch in s')
    # plt.ylabel('Acceleration Norm km/s^2')
    # plt.title('Acceleration Norm')



    # plt.figure()
    plt.subplot(311)
    plt.plot(time_seconds[:-1],doppler)
    plt.grid()
    #plt.xlabel('Time since epoch in s')
    plt.ylabel('Doppler Hz')
    plt.title('Doppler Hz')
    plt.legend(legend_list,loc='best')
    plt.subplot(312)
    plt.plot(time_seconds[:-2], np.diff(doppler)/time_step)
    plt.grid()
    #plt.xlabel('Time since epoch in s')
    plt.ylabel('Chirp Hz/second')
    plt.title('Chirp Hz')
    plt.legend(legend_list,loc='best')
    plt.subplot(313)
    #plt.plot(time_seconds,altitude_array)
    #plt.xlabel('Time since epoch in s')
    #plt.ylabel('Elevation Degrees')
    #plt.title('Elevation')
    plt.plot(time_seconds,distance_array)
    plt.grid()
    plt.xlabel('Time since epoch in s')
    plt.ylabel('Range in km')
    plt.title('Range km')
    plt.legend(legend_list,loc='best')
    percent_visable = 100.0*np.sum(altitude_array>0)/float(len(altitude_array))
    print "Can see this satellite {}% of the day".format(round(percent_visable,2))
    a_dict = OrderedDict()
    a_dict['Time'] = np.array(time_seconds[:-1],dtype='float64')
    a_dict['delay(s)'] = 1000.0*distance_array[:-1]/np.array([speed_of_light],dtype='float64')
    a_dict['Doppler(Hz)'] = np.array(doppler,dtype='float64')
    a_dict['Gain(dB)'] = np.zeros(len(doppler),dtype='float64')
    a_dict['Noise (dBm)'] = -168.0*np.ones(len(doppler),dtype='float64')
    df = pd.DataFrame(a_dict)
    df.to_csv('realistic_gps_profile.csv',index=False)



    plt.show()



print(satellite)
