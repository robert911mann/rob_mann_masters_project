// Standard libraries
#include <string>       // string
#include <iostream>     // cout, cerr, fixed, endl
#include <iomanip>      // setprecision
#include <fstream>      // ifstream, ofstream
#include <cmath>

// CUDA libraries
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/sequence.h>
#include <thrust/functional.h>
#include <thrust/extrema.h>


#include <cufft.h>


#define PI 3.141592654f

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::setprecision;
using std::ifstream;
using std::ofstream;
using std::ios;
using std::ios_base;


int read_complex_float_file(string filename,
                            int num_samples, 
                            thrust::host_vector< cuFloatComplex > & S_out) {

    ifstream file (filename.c_str(), ios::in|ios::binary|ios::ate);
    

    std::streampos file_size_bytes;
    if (file.is_open()) {
        file_size_bytes = file.tellg();
        if (file_size_bytes < num_samples*8) {
            cerr << "File 1 does not contain enough samples: " << file_size_bytes/8;
            cerr << "<" << num_samples <<endl;
            return -1;
        }
        file.seekg(0, ios::beg);
        file.read((char*)&S_out[0],num_samples*8);
        file.close();
    }
    else {
        cerr << "Failed to open file : " << filename << endl;
        return -1;
    }
    return 0;
}

int write_float_file(string filename,
                     int num_samples, 
                     const thrust::host_vector<float> & S_in
                    ) {
    
    ofstream file (filename.c_str(), ios::out|ios::binary);
    
    if (file.is_open()) {
        file.write((char*)&S_in[0],num_samples*4);
    }
    else {
        cerr << "Failed to open file : " << filename << endl;
        return -1;
    }
return 0;
}

int append_float_file(string filename,
                     int num_samples, 
                     const thrust::host_vector<float> & S_in
                    ) {
    
    ofstream file (filename.c_str(), ios::out|ios::binary|ios_base::app);
    
    if (file.is_open()) {
        file.write((char*)&S_in[0],num_samples*4);
    }
    else {
        cerr << "Failed to open file : " << filename << endl;
        return -1;
    }
return 0;
}

struct cmplx_conj
{
    __host__ __device__
        cuFloatComplex operator() (const cuFloatComplex& x) const {
            return cuConjf(x);
    }
};



struct cmplx_mul
{
    __host__ __device__
        cuFloatComplex operator() (const cuFloatComplex& x1, const cuFloatComplex& x2) const {
            return cuCmulf(x1,x2);
    }
};

struct cmplx_abs
{
    __host__ __device__
        float operator() (const cuFloatComplex& x) const {
            return cuCabsf(x);
    }
};

struct cmplx_freq_shift : public thrust::binary_function<float,cuFloatComplex,cuFloatComplex>
{
    const float two_pi_k;

    cmplx_freq_shift(float _two_pi_k) : two_pi_k(_two_pi_k) {}

    __device__ __host__
        cuFloatComplex operator()(const float& index_n_by_N, const cuFloatComplex& x_in) const { 
            // e^(-1j*2*pi*k*n/N)=cos(2*pi*k*n/N)+1j*sin(2*pi*k*n/N)
            // 2*pi*k=two_pi_k
            // n/N=index_n_by_N
            // theta = two_pi_k*index_n_by_N
            // e^(j*theta)=cos(theta)+1j*sin(theta) via euler's
            float theta = two_pi_k*index_n_by_N;
            
            //return k * index_n + x_in;
            return cuCmulf(make_cuFloatComplex(cosf(theta),sinf(theta)),x_in);
        }
};

void fftshift(thrust::device_vector< float > x_in,thrust::device_vector< float > &x_out) {
    double length = x_in.size();
    size_t length_by_two = std::ceil(length/2);
    thrust::copy_n(x_in.begin()+length_by_two,length_by_two,x_out.begin());
    thrust::copy_n(x_in.begin(),length_by_two,x_out.begin()+length_by_two);

    return;
}

void copy_part_of_row_relative_to_zero(thrust::device_vector<float> x_in, size_t center, size_t radius, thrust::host_vector<float> &y_out) {
    double length = x_in.size();
    size_t length_by_two = std::ceil(length/2);
    thrust::copy_n(x_in.begin()+length_by_two+center-radius,2*radius+1,y_out.begin());
    return;
}

void copy_part_of_row_relative_to_zero(thrust::device_vector<float> x_in, size_t center, size_t radius, thrust::host_vector<float>::iterator y_out) {
    double length = x_in.size();
    size_t length_by_two = std::ceil(length/2);
    thrust::copy_n(x_in.begin()+length_by_two+center-radius,2*radius+1,y_out);
    return;
}

float calculate_noise_power(thrust::host_vector<float>::iterator x_start,
            thrust::host_vector<float>::iterator x_end) {
    unsigned long length = x_end - x_start;
    thrust::host_vector<double> x_sorted(length);
    thrust::copy(x_start,x_end,x_sorted.begin());
    thrust::sort(x_sorted.begin(),x_sorted.end());
    // x_in is always odd
    unsigned long int median_index = x_sorted.size()/2;
    float power = x_sorted[median_index]*x_sorted[median_index];
    return power;
}

int main(int argc, char *argv[])
{
    if (argc < 7) {
        std::cerr << argv[0] << " <filename1> <filename2> <sampling rate>";
        cerr <<" <integration time> <tdoa radius>";
        cerr <<" <fdoa radius>"<< endl;
        return -1;
    }
    // Parse arguments  
    cout << "Arguments" << endl; 
    string filename1 = argv[1];
    cout << "<filename1> " << filename1 << endl;
    string filename2 = argv[2];
    cout << "<filename2> " << filename2 << endl;
    float fs_hz = atof(argv[3]);
    cout << "<sampling rate> " << std::fixed << setprecision(1) << fs_hz << " Hz" << endl;
    float int_time = atof(argv[4]);
    cout << "<integration time> " << std::fixed << setprecision(6) << int_time << " seconds" << endl;
   float tdoa_radius_s = atof(argv[5]);
    cout << "<tdoa radius> " <<  std::fixed << setprecision(6) << tdoa_radius_s << " seconds" << endl;
   float fdoa_radius_hz= atof(argv[6]);
   cout << "<fdoa radius> " <<  std::fixed << setprecision(6) << fdoa_radius_hz << " Hertz" << endl;



    // Hardcoded search center parameters
    cout << endl;
    cout << "Hardcoded search parameters" << endl;
    float tdoa_center_s = 0.0F;
    cout << "<tdoa center> " << std::fixed << setprecision(6) << tdoa_center_s << " seconds" << endl;
    float fdoa_center_hz = 0.0F;
    cout << "<fdoa center> " << std::fixed << setprecision(6) << fdoa_center_hz << " Hertz" << endl;
 
    // Convert arguments to discrete
    cout << endl;
    cout << "Discrete parameters derived from arguments" << endl;
    int num_samples = (int) (int_time*fs_hz);
    cout << "<number of samples> " << num_samples << " samples" << endl;
    int tdoa_radius_samples = (int) (tdoa_radius_s*fs_hz);  
    cout << "<TDOA radius samples> " << tdoa_radius_samples << " samples" << endl;
    float fdoa_res = fs_hz/(float) num_samples;
    cout << "<FDOA resolution> " << fdoa_res << " Hz/bin" << endl;
    int fdoa_radius_samples = (int) (fdoa_radius_hz/fdoa_res);
    cout << "<FDOA radius samples> " << fdoa_radius_samples << " samples" << endl;
    int fdoa_diameter_samples = 2*fdoa_radius_samples+1;
    int tdoa_diameter_samples = 2*tdoa_radius_samples+1;
    long total_size = fdoa_diameter_samples*tdoa_diameter_samples;

    // Hardcoded search center parameters converted to discrete
    cout << endl;
    cout << "Hardcoded search parameters converted to discrete" << endl;
    int tdoa_center_samples = (int) (tdoa_center_s*fs_hz);
    cout << "<TDOA center samples> " << tdoa_center_samples << " samples" << endl;

    int fdoa_center_samples = (int) (fdoa_center_hz/fdoa_res);

    cout << "<FDOA center samples> " << fdoa_center_samples << " samples" << endl;

    thrust::host_vector< cuFloatComplex > S1_host(num_samples);
    thrust::host_vector< cuFloatComplex > S2_host(num_samples);
    thrust::host_vector< float > abs_corr_rotate_host(2*tdoa_radius_samples+1);
    thrust::host_vector< float > surface_host(
        (2*fdoa_radius_samples+1)*(2*tdoa_radius_samples+1));
    thrust::host_vector< float > tdoas_vector(2*tdoa_radius_samples+1);
    thrust::host_vector< float > fdoas_vector(2*fdoa_radius_samples+1);

    
    int read_success = read_complex_float_file(filename1,num_samples,S1_host);
    if (read_success != 0) {
        return -1;
    }
    read_success = read_complex_float_file(filename2,num_samples,S2_host);
    if (read_success != 0) {
        return -1;
    }


    thrust::device_vector< cuFloatComplex > S1_device = S1_host;
    thrust::device_vector< cuFloatComplex > S1_device_copy;
    thrust::device_vector< cuFloatComplex > S2_device = S2_host;
    thrust::device_vector< cuFloatComplex > Corr(num_samples);
    thrust::device_vector< float > abs_corr(num_samples);
    thrust::device_vector< float > abs_corr_rotate(num_samples);
    thrust::device_vector< float > indexes_by_N(num_samples);
    string corr_file_name = "caf_surface.bin";
    string tdoa_axis_name = "tdoa_axis.bin";
    string fdoa_axis_name = "fdoa_axis.bin";
    string caf_peak_file_name = "caf_peak.json";

    cufftHandle plan;
    cufftPlan1d(&plan, num_samples, CUFFT_C2C, 1);
    cmplx_conj conj_op;
    cmplx_mul mul_op;
    cmplx_abs abs_op;


    // miscillenius setup
    thrust::sequence(indexes_by_N.begin(), indexes_by_N.end());
    thrust::transform(indexes_by_N.begin(), indexes_by_N.end(), 
        indexes_by_N.begin(), thrust::placeholders::_1/num_samples);

    thrust::sequence(tdoas_vector.begin(), tdoas_vector.end());
    thrust::transform(tdoas_vector.begin(), tdoas_vector.end(),tdoas_vector.begin(), 
        (thrust::placeholders::_1+tdoa_center_samples-tdoa_radius_samples)/fs_hz);

    thrust::sequence(fdoas_vector.begin(), fdoas_vector.end());
    thrust::transform(fdoas_vector.begin(), fdoas_vector.end(),fdoas_vector.begin(), 
        (thrust::placeholders::_1+fdoa_center_samples-fdoa_radius_samples)*fdoa_res);


    // FFT(S2)    
    cufftExecC2C(plan, thrust::raw_pointer_cast(S2_device.data()),
                 thrust::raw_pointer_cast(S2_device.data()), CUFFT_FORWARD);
    // CONJUGATE S2_FFT
    thrust::transform(S2_device.begin(),S2_device.end(),S2_device.begin(),conj_op);
    
    cout<<endl;
    
    for (int ii=0; ii<2*fdoa_radius_samples+1;ii++) {
        cout << 100.0F*(float)ii/(float)(2*fdoa_radius_samples+1) << " %\r";
        std::cout.flush();
        S1_device_copy = S1_device;
        float kk = ii - fdoa_radius_samples;
        // S1*e^(j*2*pi*fdoa*t)
        thrust::transform(indexes_by_N.begin(), indexes_by_N.end(), 
            S1_device_copy.begin(), S1_device_copy.begin(),cmplx_freq_shift(2.0F*PI*kk));
        cufftExecC2C(plan, thrust::raw_pointer_cast(S1_device_copy.data()), 
                 thrust::raw_pointer_cast(S1_device_copy.data()), CUFFT_FORWARD);
        // Corr = S1_FFT*S2_FFT
        thrust::transform(S1_device_copy.begin(),S1_device_copy.end(),S2_device.begin(),
            Corr.begin(),mul_op);
        // IFFT(S1_FFT*S2_FFT)
        cufftExecC2C(plan, thrust::raw_pointer_cast(Corr.data()), 
            thrust::raw_pointer_cast(Corr.data()), CUFFT_INVERSE);
        // abs_corr
        thrust::transform(Corr.begin(),Corr.end(),abs_corr.begin(),abs_op);

        fftshift(abs_corr,abs_corr_rotate);

        copy_part_of_row_relative_to_zero(abs_corr_rotate,tdoa_center_samples,
            tdoa_radius_samples, surface_host.begin()+ii*tdoa_diameter_samples);
    }
    write_float_file(corr_file_name,total_size,surface_host);


    //thrust::device_vector<float>::iterator  max_iter = ;
    thrust::host_vector<float>::iterator max_iter = thrust::max_element(surface_host.begin(), surface_host.end());
    long max_index = max_iter - surface_host.begin();
    //long tdoa_index = max_index / fdoa_diameter_samples;
    long fdoa_index = max_index / tdoa_diameter_samples;
    long tdoa_index = max_index % tdoa_diameter_samples;
    float tdoa_s = tdoas_vector[tdoa_index];
    float fdoa_hz = fdoas_vector[fdoa_index];
    cout << "tdoa "<< tdoa_s*1e6 << " us"<< endl;
    cout << "fdoa "<< fdoa_hz << " Hz"<<  endl;

    
    write_float_file(tdoa_axis_name,2*tdoa_radius_samples+1,tdoas_vector);
    write_float_file(fdoa_axis_name,2*fdoa_radius_samples+1,fdoas_vector);
    cout << fdoa_index*tdoa_diameter_samples << " to " << (fdoa_index+1)*tdoa_diameter_samples << endl;
    float signal_power = max_iter[0]*max_iter[0];
    float noise_power = calculate_noise_power(surface_host.begin()+fdoa_index*tdoa_diameter_samples,
        surface_host.begin()+(fdoa_index+1)*tdoa_diameter_samples);
    float snr = signal_power/noise_power -1;
    float snr_db = 10*std::log10(snr);
    cout << "SNR " << snr_db << " dB" << endl;
    
    
    

    ofstream caf_peak_file (caf_peak_file_name.c_str());
    if (caf_peak_file.is_open())
    {
        caf_peak_file << "{"<< endl; 
        caf_peak_file << setprecision(9) << "\"tdoa_s\"  : " << tdoa_s  << ","<< endl;
        caf_peak_file << setprecision(9) << "\"fdoa_hz\" : " << fdoa_hz << "," << endl;
        caf_peak_file << setprecision(9) << "\"SNR_dB\"  : " << snr_db << endl;
    caf_peak_file << "}"<< endl;
        caf_peak_file.close();
    }
        else {
        cerr << "Failed to open file : " << caf_peak_file_name << endl;
        return -1;
    }





    return 0;
}
