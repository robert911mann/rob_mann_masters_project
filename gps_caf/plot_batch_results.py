import argparse
import json
import os
import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser(description="plot IQ data")
parser.add_argument("--int_time", type=float,default=.002048)
parser.add_argument("--samp_rate", type=float,default=1e6)
parser.add_argument("--folder", type=str,default="static_2microseconds")
args = parser.parse_args()
length=int(args.int_time*args.samp_rate)
folder = args.folder
time_fname = "time_axis.json"
with open(os.path.join(folder,time_fname),'r') as a_file:
    time_axis_seconds = json.load(a_file)['time_axis_seconds']

results_fname = "results_{}.json".format(length)
with open(os.path.join(folder,results_fname),'r') as a_file:
    a_dict = json.load(a_file)
SNR_dB_list = a_dict["SNR_dB_list"]
tdoa_s_list = a_dict["tdoa_s_list"]
fdoa_hz_list = a_dict["fdoa_hz_list"]

plt.subplot(311)
plt.plot(time_axis_seconds,SNR_dB_list)
plt.grid()
plt.xlabel('Time s')
plt.ylabel('SNR dB')


plt.subplot(312)
plt.plot(time_axis_seconds,tdoa_s_list*np.array([1e6]))
plt.grid()
plt.xlabel('Time s')
plt.ylabel('TOA us')

plt.subplot(313)
plt.plot(time_axis_seconds,fdoa_hz_list)
plt.grid()
plt.xlabel('Time s')
plt.ylabel('FOA Hz')

fig_name = os.path.join(folder,'results_{}.png'.format(length))
plt.savefig(fig_name)
plt.show()
