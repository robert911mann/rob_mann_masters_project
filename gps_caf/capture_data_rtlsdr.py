#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Capture Data Rtlsdr
# Generated: Mon Nov 19 19:38:10 2018
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import ConfigParser
import osmosdr
import time


class capture_data_rtlsdr(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Capture Data Rtlsdr")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 1000000
        self._gain_config = ConfigParser.ConfigParser()
        self._gain_config.read("gain.cfg")
        try: gain = self._gain_config.getfloat("main", "gain")
        except: gain = 64
        self.gain = gain
        self._default_tx_on_off_config = ConfigParser.ConfigParser()
        self._default_tx_on_off_config.read("gain.cfg")
        try: default_tx_on_off = self._default_tx_on_off_config.getfloat("main", "default_tx_on_off")
        except: default_tx_on_off = 1
        self.default_tx_on_off = default_tx_on_off
        self._default_num_samples_config = ConfigParser.ConfigParser()
        self._default_num_samples_config.read("gain.cfg")
        try: default_num_samples = self._default_num_samples_config.getint("main", "default_num_samples")
        except: default_num_samples = 1048576
        self.default_num_samples = default_num_samples
        self.capture_frequency = capture_frequency = 70e6

        ##################################################
        # Blocks
        ##################################################
        self.rtlsdr_source_0 = osmosdr.source( args="numchan=" + str(1) + " " + "" )
        self.rtlsdr_source_0.set_sample_rate(samp_rate)
        self.rtlsdr_source_0.set_center_freq(int(capture_frequency), 0)
        self.rtlsdr_source_0.set_freq_corr(0, 0)
        self.rtlsdr_source_0.set_dc_offset_mode(2, 0)
        self.rtlsdr_source_0.set_iq_balance_mode(0, 0)
        self.rtlsdr_source_0.set_gain_mode(False, 0)
        self.rtlsdr_source_0.set_gain(gain, 0)
        self.rtlsdr_source_0.set_if_gain(20, 0)
        self.rtlsdr_source_0.set_bb_gain(20, 0)
        self.rtlsdr_source_0.set_antenna("", 0)
        self.rtlsdr_source_0.set_bandwidth(2*samp_rate, 0)
          
        self.blocks_skiphead_0 = blocks.skiphead(gr.sizeof_gr_complex*1, 100000)
        self.blocks_head_0 = blocks.head(gr.sizeof_gr_complex*1, default_num_samples)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, "gold1_plus_gold2_capture.dat", False)
        self.blocks_file_sink_0.set_unbuffered(False)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_head_0, 0), (self.blocks_file_sink_0, 0))    
        self.connect((self.blocks_skiphead_0, 0), (self.blocks_head_0, 0))    
        self.connect((self.rtlsdr_source_0, 0), (self.blocks_skiphead_0, 0))    

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.rtlsdr_source_0.set_sample_rate(self.samp_rate)
        self.rtlsdr_source_0.set_bandwidth(2*self.samp_rate, 0)

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.rtlsdr_source_0.set_gain(self.gain, 0)

    def get_default_tx_on_off(self):
        return self.default_tx_on_off

    def set_default_tx_on_off(self, default_tx_on_off):
        self.default_tx_on_off = default_tx_on_off
        self._default_tx_on_off_config = ConfigParser.ConfigParser()
        self._default_tx_on_off_config.read("gain.cfg")
        if not self._default_tx_on_off_config.has_section("main"):
        	self._default_tx_on_off_config.add_section("main")
        self._default_tx_on_off_config.set("main", "default_tx_on_off", str(self.default_tx_on_off))
        self._default_tx_on_off_config.write(open("gain.cfg", 'w'))

    def get_default_num_samples(self):
        return self.default_num_samples

    def set_default_num_samples(self, default_num_samples):
        self.default_num_samples = default_num_samples
        self.blocks_head_0.set_length(self.default_num_samples)

    def get_capture_frequency(self):
        return self.capture_frequency

    def set_capture_frequency(self, capture_frequency):
        self.capture_frequency = capture_frequency
        self.rtlsdr_source_0.set_center_freq(int(self.capture_frequency), 0)


def main(top_block_cls=capture_data_rtlsdr, options=None):

    tb = top_block_cls()
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
