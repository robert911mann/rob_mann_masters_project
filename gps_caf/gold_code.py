import numpy as np
import matplotlib.pyplot as plt
import sk_dsp_comm.digitalcom as dc
def load_gold_rz(gold_num=10,bits_out=1500):
    ca = np.loadtxt('ca1thru37.txt', dtype=np.float32, usecols=(gold_num - 1,), unpack=True)
    bits_in_one_sequence = len(ca)
    num_complete_sequences = bits_out//bits_in_one_sequence
    remainder = bits_out%bits_in_one_sequence
    ret_array = np.zeros([bits_out])
    for ii in range(num_complete_sequences):
        ret_array[ii*bits_in_one_sequence:(ii+1)*bits_in_one_sequence] = ca
    ret_array[(num_complete_sequences)*bits_in_one_sequence:(num_complete_sequences)*bits_in_one_sequence+remainder] = ca[:remainder]
    return ret_array

def load_gold(gold_num=10,bits_out=1500):
    ret_array = load_gold_rz(gold_num=gold_num,bits_out=bits_out)
    ret_array=ret_array*2-1
    return ret_array

def delay(x_x, delay=2e-6, fs=1e6):
    real_fs = fs * 1
    y_y = np.zeros_like(x_x)
    if delay > 0:
        delay_samples = int(delay*real_fs)

        y_y[delay_samples:] = x_x[:-delay_samples]
    elif delay < 0:
        delay_samples = -int(delay*real_fs)
        y_y[:-delay_samples] = x_x[delay_samples:]
    else:
        y_y[:]=x_x[:]
    print delay_samples, "samples" 
    print delay_samples*1e6/real_fs, "microseconds"
    return y_y

def modulate(data, mod_freq = 70e6, fs=1e6):
    tt = np.arange(len(data))
    complex_sinusoid = np.exp(1j*2*np.pi*tt*mod_freq/fs)
    return data*complex_sinusoid

def calculate_power(signal):
    instantaneous= signal**2
    return np.sum(instantaneous)/len(instantaneous)
if __name__ == '__main__':
    length = int(2e6)
    prn_1 = load_gold(1,length)
    prn_2 = load_gold(2,length)
    prn_1_delay = delay(prn_1,100e-6,fs=1e6)
    prn_2_delay = delay(prn_2,300e-6,fs=1e6)
    noise = np.sqrt(10)*np.random.randn(np.size(prn_1_delay))
    combined=prn_1_delay+prn_2_delay+noise
    print calculate_power(prn_1_delay),'Watts'
    print calculate_power(prn_2_delay),'Watts'
    print calculate_power(noise),'Watts'

    #dc.NRZ_bits()


    corr,taus = dc.xcorr(prn_1,prn_1_delay,1024)
    corr_dis,taus_dis = dc.xcorr(combined,prn_1,1024)
    corr_dis2,taus_dis2 = dc.xcorr(combined,prn_2,1024)
    corr_dis = np.abs(corr_dis)
    corr_dis2 = np.abs(corr_dis2)
    plt.figure()
    #plt.subplot(211)
    plt.plot(taus_dis,corr_dis)#/max(corr_dis))
    plt.plot(taus_dis2,corr_dis2)#/max(corr_dis2))
    #plt.subplot(212)
    #plt.plot(taus_dis,20*np.log10(np.abs(corr_dis)))
    #plt.ylim(-.1,1.1)
    #plt.figure()
    #plt.plot(prn_1_delay)
    #plt.grid()

    #plt.figure()
    fdop=-5
    doppler = np.exp(1j*2*np.pi*np.arange(length)/1e6*fdop,dtype='complex64')
    #plt.plot(doppler.real)


    combined_float = np.complex64(prn_1_delay+prn_2_delay)
    combined_float.tofile("gold1_plus_gold2_pristine.dat")

    combined_float = np.complex64(prn_1_delay+prn_2_delay*doppler+noise)
    combined_float.tofile("gold1_plus_gold2_capture.dat")


    combined_prn_1 = np.complex64(prn_1)
    combined_prn_1.tofile("gold1.dat")


    combined_prn_2 = np.complex64(prn_2)
    combined_prn_2.tofile("gold2.dat")



    plt.show()
