import numpy as np
import matplotlib.pyplot as plt
surf = np.fromfile("caf_surface.bin",'float32')
tdoa_axis = np.fromfile("tdoa_axis.bin",'float32')
fdoa_axis = np.fromfile("fdoa_axis.bin",'float32')



max_index = np.argmax(surf)
surf.resize(len(fdoa_axis),len(tdoa_axis))

fdoa_index,tdoa_index = np.unravel_index(max_index,(len(fdoa_axis),len(tdoa_axis)))


signal_noise_power = np.var(surf[fdoa_index,:])
signal_power = surf[fdoa_index,tdoa_index]**2
noise_power = np.median(surf[fdoa_index,:])**2
snr = signal_power/noise_power-1
tdoa = tdoa_axis[tdoa_index]
fdoa = fdoa_axis[fdoa_index]
surf = surf/np.max(surf)
surf_dB = 20*np.log10(surf)


plt.plot(tdoa_axis*1e6,surf_dB[fdoa_index,:],'b')
plt.xlabel('TDOA us')
plt.ylabel('Correlation dB')
plt.grid()

plt.figure()
plt.plot(fdoa_axis,surf_dB[:,tdoa_index],'g')
plt.xlabel('FDOA Hz')
plt.ylabel('Correlation dB')
plt.grid()

plt.figure()
plt.contourf(tdoa_axis*1e6,fdoa_axis,surf)
plt.xlabel('TDOA us')
plt.ylabel('FDOA Hz')


plt.show()


